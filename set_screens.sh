#!/bin/bash
xrandr --output DP-1 --primary --mode 3840x2160 --rate 60
xrandr --output DP-2 --left-of DP-1 --mode 1920x1080 --rate 144.00
xrandr --output HDMI-1 --right-of DP-1 --mode 1920x1080 --rate 60

